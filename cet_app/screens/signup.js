import React, {Component} from 'react';
import { ScrollView, StyleSheet,View,TouchableOpacity,TextInput,Text  } from 'react-native';
import Logo from '../logo';

export default class SignupScreen extends Component{
  render(){
    const {navigate} = this.props.navigation;
    return(
      <View style={styles.container}>
        <Logo/>
        <View >
            <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder="Name"
              placeholderTextColor = "white"
              selectionColor="white"
              onSubmitEditing={()=> this.email.focus()}
              />
            <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder="Email"
              placeholderTextColor = "white"
              selectionColor="white"
              keyboardType="email-address"
              ref={(input) => this.email = input}
              onSubmitEditing={()=> this.password.focus()}
              />
            <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder="Password"
              placeholderTextColor = "white"
              selectionColor="white"
              secureTextEntry={true}
              ref={(input) => this.password = input}
              autoCorrect={false}
              onSubmitEditing={()=> this.confirmpassword.focus()}
              />
            <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder="Confirm Password"
              secureTextEntry={true}
              placeholderTextColor = "white"
              autoCorrect={false}
              ref={(input) => this.confirmpassword = input}
              />  
            <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('Home')}>
             <Text style={styles.buttonText}>SignUp</Text>
            </TouchableOpacity>     
  		</View>
      <View style={styles.signupTextCont}>
					<Text style={styles.signupText}>Already have an account?</Text>
          <TouchableOpacity onPress={() => navigate('LoginScreen') }><Text style={styles.signupButton}> Sign in</Text></TouchableOpacity>
				</View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container : {
    flexGrow: 1,
    justifyContent:'center',
    alignItems: 'center',
    marginHorizontal:40,
    marginVertical:40,
    backgroundColor:'#ffffff'
  },

  inputBox: {
    width:300,
    height:35,
    backgroundColor:'black',
    borderRadius: 25,
    paddingHorizontal:16,
    fontSize:16,
    color:'white',
    marginVertical: 10
  },
  button: {
    width:300,
    backgroundColor:'#1c313a',
     borderRadius: 25,
      marginVertical: 10,
      paddingVertical: 13
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'black',
    textAlign:'center'
  },
  signupTextCont : {
  	flexGrow: 1,
    alignItems:'flex-end',
    justifyContent :'center',
    paddingVertical:16,
    flexDirection:'row'
  },
  signupText: {
  	color:'black',
  	fontSize:16
  },
  signupButton: {
  	color:'blue',
  	fontSize:16,
  	fontWeight:'500'
  }

  
});
