import React , {Component} from 'react';
import { ScrollView, StyleSheet,View,TouchableOpacity,TextInput,Text } from 'react-native';
import Logo from '../logo';

export default class LoginScreen extends Component{
  constructor(props) {
    super(props);
    this.state = {
        username:'',
        password:'',
        loginMainCheck: true,
        showMessageCheck:false,
        showMessage:'',
        loginSuccess:false,
    };
    this.loginOperations = this.loginOperations.bind(this);
  }

  render(){
    const {navigate} = this.props.navigation;
    return(
      <View style={styles.container} > 
        <Logo />
        <View>
        <TextInput style={styles.inputBox} 
              underlineColorAndroid='transparent' 
              placeholder="Email"
              placeholderTextColor = "white"
              selectionColor="white"
              keyboardType="email-address"
              onSubmitEditing={()=> this.password.focus()}
              />
        <TextInput style={styles.inputBox} 
              underlineColorAndroid='transparent' 
              placeholder="Password"
              secureTextEntry={true}
              placeholderTextColor = "white"
              ref={(input) => this.password = input}
              />  
          <TouchableOpacity style={styles.button} onPress={() => this.loginOperations} >
             <Text style={styles.buttonText}>Login</Text>
          </TouchableOpacity> 
        </View>
        <View style={styles.signupTextCont}>
					<Text style={styles.signupText}>Don't have an account yet?</Text>
          <TouchableOpacity onPress={() => navigate('SignupScreen')}><Text style={styles.signupButton}> Signup</Text></TouchableOpacity>
				</View>
      </View>
    )
  }

  loginOperations() {
    let user = this.state.username, pass=this.state.pass;
    if(user.length==0 || pass.length==0){
        this.setState({showMessage:'Please fill both the fields.', showMessageCheck:true});
    }
    else{
        this.setState({showMessageCheck:false});
    fetch('', {
        method:'POST',
        headers: {
            'Accept':'application/json',
            'Content-Type':'application/json'
        },
        body: JSON.stringify({
            'username':user,
            'password':pass,
        }),
    })
    .then(resData => resData.json())
    .then(res => {
        console.warn('Received as '+res);
        this.setState({showMessage:'Succesfully Logged In. Welcome '+res['first_name']+' '+res['second_name'],
            showMessageCheck:true, loginSuccess:true
            });
            // this.props.navigation.navigate('Home')
    })
    .catch(err => {
        this.setState({showMessage:'Logged In Unsuccessful',
        showMessageCheck:true, loginSuccess:false
        });
    })
    }
}

}

const styles = StyleSheet.create({
  container:{
    marginHorizontal:20,
    marginVertical:50,
    width:350,
    backgroundColor:'#ffffff'

  },
  inputBox:{
    width:300,
    backgroundColor:'black',
    borderColor:'black',
    borderRadius: 25,
    paddingHorizontal:16,
    fontSize:16,
    color:'white',
    marginVertical: 10,
    height:35,
    alignItems:'center',
    marginHorizontal:20

  },
  button:{
    width:300,
    backgroundColor:'#1c313a',
     borderRadius: 25,
      marginVertical: 10,
      paddingVertical: 13,
      alignItems:'center',
      marginHorizontal:20

  },
  buttonText:{
    fontSize:16,
    fontWeight:'500',
    color:'white',
    textAlign:'center'

  },
  signupTextCont : {
  	flexGrow: 1,
    alignItems:'flex-end',
    justifyContent :'center',
    paddingVertical:16,
    flexDirection:'row'
  },
  signupText: {
  	color:'black',
  	fontSize:16
  },
  signupButton: {
  	color:'blue',
  	fontSize:16,
  	fontWeight:'500'
  }
})