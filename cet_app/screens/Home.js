import React,{Component} from 'react';
import {View,
StyleSheet,
TouchableOpacity,Button,Text} from 'react-native';
import Logo from '../logo';

export default class Home extends Component{
  render(){
    const {navigate} = this.props.navigation;
    return(
      <View style={styles.container}>
          <Logo />
          <TouchableOpacity 
          style={styles.button}
          onPress= {() => navigate('LoginScreen')}>
          <Text style={styles.buttonText}>Login</Text>
          </TouchableOpacity>
          
          <TouchableOpacity 
          style={styles.button}
          onPress = {() => navigate('SignupScreen')}>
            <Text style={styles.buttonText}>
              Signup
            </Text>
          </TouchableOpacity>
      </View>
    )
  };
}

const styles = StyleSheet.create({
  container:{
    justifyContent:'center',
    marginTop:150,
    alignItems:'center',
    alignContent:'center',
    backgroundColor:'#ffffff'
  },
  button: {
    width:300,
    backgroundColor:'#1c313a',
     borderRadius: 25,
      marginVertical: 10,
      paddingVertical: 13,
      justifyContent:'center',
      marginHorizontal:50
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'white',
    textAlign:'center'
  }
});