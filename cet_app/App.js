import React, {Component} from 'react';
import {Stylesheet} from 'react-native';
import {Navigator} from './router';

export default class App extends Component{
  render(){
    return(
      <Navigator />
    );
  }
}
