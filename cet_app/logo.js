import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
   Image 
} from 'react-native';

export default class Logo extends Component{
	render(){
		return(
			<View style={styles.container}>
				<Image  style={{width:120, height: 120}}
          			source={require('./logo.png')}/>
          		<Text style={styles.logoText}>Welcome to College Of Engineering And Technology.</Text>	
  			</View>
			)
	}
}

const styles = StyleSheet.create({
  container : {
    flexGrow: 1,
    alignItems: 'center'
  },
  logoText : {
		marginVertical: 25,
		alignItems:'center',
  	fontSize:18,
		color:'black',
		justifyContent:'center',
		marginHorizontal:30
  }
});