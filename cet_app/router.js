import React from 'react';
import {createStackNavigator, createBottomTabNavigator} from 'react-navigation';
import LoginScreen from './screens/login';
import SignupScreen from './screens/signup';
import Home from './screens/Home';

export const Navigator = createStackNavigator({
    LoginScreen:{
        screen:LoginScreen,
        navigationOptions:{
            title:'Login Screen',
            header:null
        },
    },
    SignupScreen:{
        screen:SignupScreen,
        navigationOptions:{
            title:'SignUp Screen',
            header:null
        },
    },
    Home:{
        screen:Home,
        navigationOptions:{
            title:'Home',
            header:null
        },
    },
},
    {
        initialRouteName:'Home',
    }
)
